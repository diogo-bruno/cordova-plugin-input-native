// 
// Decompiled by Procyon v0.5.36
// 

package nl.xservices.plugins.nativekeyboard.lib;

import android.widget.TextView;
import android.view.ViewGroup;
import android.view.View;
import android.graphics.Typeface;
import android.content.Context;

class FontManager
{
    private static final String ROOT = "fonts/";
    static final String FONTAWESOME = "fonts/fontawesome-webfont.ttf";
    static final String IONICONS = "fonts/ionicons.ttf";
    
    static Typeface getTypeface(final Context context, final String s) {
        try {
            return Typeface.createFromAsset(context.getAssets(), s);
        }
        catch (RuntimeException ex) {
            return null;
        }
    }
    
    private static void markAsIconContainer(final View view, final Typeface typeface) {
        if (view instanceof ViewGroup) {
            final ViewGroup viewGroup = (ViewGroup)view;
            for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                markAsIconContainer(viewGroup.getChildAt(i), typeface);
            }
        }
        else if (view instanceof TextView) {
            ((TextView)view).setTypeface(typeface);
        }
    }
}
