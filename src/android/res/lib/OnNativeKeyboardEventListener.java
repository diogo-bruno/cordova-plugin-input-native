// 
// Decompiled by Procyon v0.5.36
// 

package nl.xservices.plugins.nativekeyboard.lib;

import org.json.JSONObject;

public interface OnNativeKeyboardEventListener
{
    void onSuccess(final JSONObject p0);
    
    void onError(final String p0);
}
