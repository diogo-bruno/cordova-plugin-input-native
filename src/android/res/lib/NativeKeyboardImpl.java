// 
// Decompiled by Procyon v0.5.36
// 

package nl.xservices.plugins.nativekeyboard.lib;

import android.view.Display;
import android.graphics.Point;
import android.graphics.Rect;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.view.MotionEvent;
import android.content.res.Resources;
import java.lang.reflect.Field;
import android.view.inputmethod.InputMethodManager;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.text.Editable;
import android.view.animation.Animation;
import android.view.animation.AlphaAnimation;
import android.text.TextWatcher;
import org.json.JSONException;
import android.text.Spanned;
import android.text.InputFilter;
import android.graphics.Color;
import android.content.Context;
import org.json.JSONObject;
import android.util.DisplayMetrics;
import android.view.ViewTreeObserver;
import android.view.View;
import android.app.Activity;
import android.graphics.Typeface;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.LinearLayout;

public class NativeKeyboardImpl
{
    private static final int MESSENGER_SLIDE_ANIMATION_MSEC = 500;
    private LinearLayout _accessorybarContainer;
    private Boolean licenseIsValid;
    private long appStartTs;
    private TextView leftButton;
    private EditText editText;
    private TextView rightButton;
    private Typeface fontAwesomeFont;
    private Typeface ioniconsFont;
    private Activity activity;
    private View view;
    private View rootView;
    private ViewTreeObserver.OnGlobalLayoutListener layoutListener;
    private float density;
    private OnNativeKeyboardEventListener _messengerEventListener;
    private int previousMessengerBarHeight;
    
    public NativeKeyboardImpl() {
        this.appStartTs = System.currentTimeMillis();
        this.previousMessengerBarHeight = 0;
    }
    
    public void init(final Activity activity, final View view) {
        this.activity = activity;
        this.view = view;
        this.fontAwesomeFont = FontManager.getTypeface(activity.getApplicationContext(), "fonts/fontawesome-webfont.ttf");
        this.ioniconsFont = FontManager.getTypeface(activity.getApplicationContext(), "fonts/ionicons.ttf");
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.density = displayMetrics.density;
        this.rootView = activity.getWindow().getDecorView().findViewById(16908290).getRootView();
        this.layoutListener = (ViewTreeObserver.OnGlobalLayoutListener)new NKOnGlobalLayoutListener();
        this.rootView.getViewTreeObserver().addOnGlobalLayoutListener(this.layoutListener);
    }
    
    private void configureEditText(final JSONObject jsonObject, final OnNativeKeyboardEventListener onNativeKeyboardEventListener) throws JSONException {
        (this.editText = new EditText((Context)this.activity)).setLines(4);
        this.editText.setSingleLine();
        this.editText.setMinLines(1);
        this.editText.setMaxLines(8);
        this.editText.setHorizontallyScrolling(false);
        this.editText.setText((CharSequence)jsonObject.optString("text"));
        this.editText.setHint((CharSequence)jsonObject.optString("placeholder"));
        this.editText.setHintTextColor(Color.parseColor(jsonObject.optString("placeholderColor", "#CCCCCC")));
        this.editText.setPadding(20, 0, 20, 20);
        this.editText.setTextColor(Color.parseColor(jsonObject.optString("textColor", "#444444")));
        this.editText.setGravity(80);
        this.editText.setBackgroundColor(Color.parseColor(jsonObject.optString("textViewBackgroundColor", "#F6F6F6")));
        try {
            final Field declaredField = TextView.class.getDeclaredField("mCursorDrawableRes");
            declaredField.setAccessible(true);
            declaredField.set(this.editText, this.activity.getResources().getIdentifier("cursor", "drawable", this.activity.getPackageName()));
        }
        catch (Exception ex) {}
        if (jsonObject.optBoolean("suppressSuggestions", true)) {
            this.editText.setInputType(671808);
        }
        else {
            this.editText.setInputType(147521);
        }
        this.editText.setImeOptions(6);
        this.editText.setTextSize(2, 16.0f);
        if (jsonObject.has("maxChars")) {
            this.editText.setFilters(new InputFilter[] { (InputFilter)new InputFilter() {
                    final /* synthetic */ int val$maxChars = jsonObject.getInt("maxChars");
                    
                    public CharSequence filter(final CharSequence charSequence, final int n, final int n2, final Spanned spanned, final int n3, final int n4) {
                        if (spanned.length() > this.val$maxChars - 1) {
                            return "";
                        }
                        return null;
                    }
                } });
        }
        if (this.rightButton != null) {
            this.editText.addOnLayoutChangeListener((View.OnLayoutChangeListener)new View.OnLayoutChangeListener() {
                public void onLayoutChange(final View view, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int n8) {
                    if (NativeKeyboardImpl.this.editText.getText().toString().length() > 0) {
                        final int n9 = (int)((n4 - n8) / NativeKeyboardImpl.this.density);
                        final int n10 = (int)(NativeKeyboardImpl.this.editText.getHeight() / NativeKeyboardImpl.this.density);
                        try {
                            onNativeKeyboardEventListener.onSuccess(new JSONObject().put("contentHeight", n10).put("contentHeightDiff", n9));
                            NativeKeyboardImpl.this.sendMessengerBarHeightChangedEvent((int)(NativeKeyboardImpl.this._accessorybarContainer.getChildAt(0).getHeight() / NativeKeyboardImpl.this.density));
                        }
                        catch (JSONException ex) {}
                    }
                }
            });
            this.editText.addTextChangedListener((TextWatcher)new TextWatcher() {
                public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
                }
                
                public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
                    NativeKeyboardImpl.this.sendTextChangedEventToListener(onNativeKeyboardEventListener);
                    if (NativeKeyboardImpl.this.editText.getText().toString().length() > 0) {
                        if (NativeKeyboardImpl.this.rightButton.getVisibility() == 4) {
                            NativeKeyboardImpl.this.rightButton.setVisibility(0);
                            final AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                            alphaAnimation.setDuration(400L);
                            NativeKeyboardImpl.this.rightButton.startAnimation((Animation)alphaAnimation);
                        }
                    }
                    else if (NativeKeyboardImpl.this.rightButton.getVisibility() == 0) {
                        NativeKeyboardImpl.this.rightButton.setVisibility(4);
                        final AlphaAnimation alphaAnimation2 = new AlphaAnimation(1.0f, 0.0f);
                        alphaAnimation2.setDuration(300L);
                        NativeKeyboardImpl.this.rightButton.startAnimation((Animation)alphaAnimation2);
                    }
                }
                
                public void afterTextChanged(final Editable editable) {
                }
            });
        }
        final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.view.getLayoutParams());
        layoutParams.weight = 1.0f;
        this.editText.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
        this.editText.getLayoutParams().height = -2;
        this.editText.setOnEditorActionListener((TextView.OnEditorActionListener)new TextView.OnEditorActionListener() {
            public boolean onEditorAction(final TextView textView, final int n, final KeyEvent keyEvent) {
                if (n == 6) {
                    NativeKeyboardImpl.this.sendEnteredTextToListener(false, onNativeKeyboardEventListener);
                }
                return false;
            }
        });
        if (jsonObject.optBoolean("showKeyboard", false)) {
            new Handler().postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    NativeKeyboardImpl.this.editText.requestFocus();
                    ((InputMethodManager)NativeKeyboardImpl.this.activity.getSystemService("input_method")).showSoftInput((View)NativeKeyboardImpl.this.editText, 1);
                    NativeKeyboardImpl.this.editText.setSelection(NativeKeyboardImpl.this.editText.getText().length());
                }
            }, 400L);
        }
    }
    
    private void configureCommonButtonProperties(final TextView textView, final JSONObject jsonObject, final OnNativeKeyboardEventListener onNativeKeyboardEventListener) throws JSONException {
        textView.setGravity(17);
        textView.setTextColor(Color.parseColor(jsonObject.optString("color", "#444444")));
        textView.setPadding(30, 20, 30, 20);
        final String optString = jsonObject.optString("value", "");
        final String optString2 = jsonObject.optString("type", "text");
        if ("fa".equalsIgnoreCase(optString2) || "fontawesome".equalsIgnoreCase(optString2)) {
            if (this.fontAwesomeFont == null) {
                onNativeKeyboardEventListener.onError("FontAwesome font not found.");
            }
            else {
                textView.setTypeface(this.fontAwesomeFont);
            }
            String string = optString;
            if (!string.startsWith("fa-")) {
                string = "fa-" + optString;
            }
            final String replace = string.replace('-', '_');
            final int identifier = this.activity.getResources().getIdentifier(replace, "string", this.activity.getPackageName());
            try {
                textView.setText((CharSequence)this.activity.getResources().getString(identifier));
            }
            catch (Resources.NotFoundException ex) {
                onNativeKeyboardEventListener.onError("Could not find fontawesome resource " + replace + ", you're probably trying to use a relatively new fa icon. Using fa-paper-plane-o as fallback.");
                textView.setText((CharSequence)this.activity.getResources().getString(this.activity.getResources().getIdentifier("fa_paper_plane_o", "string", this.activity.getPackageName())));
            }
            textView.setTextSize(2, 22.0f);
        }
        else if ("ion".equalsIgnoreCase(optString2) || "ionicon".equalsIgnoreCase(optString2)) {
            if (this.ioniconsFont == null) {
                onNativeKeyboardEventListener.onError("Ionicons font not found.");
            }
            else {
                textView.setTypeface(this.ioniconsFont);
            }
            textView.setText((CharSequence)optString);
            textView.setTextSize(2, 22.0f);
        }
        else {
            textView.setText((CharSequence)optString);
            final String optString3 = jsonObject.optString("textStyle", "normal");
            if ("bold".equals(optString3)) {
                textView.setTypeface((Typeface)null, 1);
            }
            else if ("italic".equals(optString3)) {
                textView.setTypeface((Typeface)null, 2);
            }
            textView.setTextSize(2, 16.0f);
        }
    }
    
    private void configureLeftButton(final JSONObject jsonObject, final OnNativeKeyboardEventListener onNativeKeyboardEventListener) throws JSONException {
        if (!jsonObject.has("leftButton")) {
            return;
        }
        final JSONObject jsonObject2 = jsonObject.getJSONObject("leftButton");
        this.configureCommonButtonProperties(this.leftButton = new TextView((Context)this.activity), jsonObject2, onNativeKeyboardEventListener);
        this.leftButton.setLayoutParams(new ViewGroup.LayoutParams(-2, -1));
        this.leftButton.setOnTouchListener((View.OnTouchListener)new View.OnTouchListener() {
            final /* synthetic */ boolean val$leftButtonDisabledWhenTextEntered = jsonObject2.optBoolean("disabledWhenTextEntered", false);
            
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                if (!NativeKeyboardImpl.this.checkLicense()) {
                    onNativeKeyboardEventListener.onError("No valid license found; usage of the native keyboard plugin is restricted to 5 minutes.");
                    return false;
                }
                try {
                    if (!this.val$leftButtonDisabledWhenTextEntered || NativeKeyboardImpl.this.editText.length() == 0) {
                        onNativeKeyboardEventListener.onSuccess(new JSONObject().put("messengerLeftButtonPressed", true));
                    }
                }
                catch (JSONException ex) {
                    onNativeKeyboardEventListener.onError(ex.getMessage());
                }
                return false;
            }
        });
    }
    
    private void configureRightButton(final JSONObject jsonObject, final OnNativeKeyboardEventListener onNativeKeyboardEventListener) throws JSONException {
        if (!jsonObject.has("rightButton")) {
            return;
        }
        this.configureCommonButtonProperties(this.rightButton = new TextView((Context)this.activity), jsonObject.getJSONObject("rightButton"), onNativeKeyboardEventListener);
        if (!jsonObject.has("text")) {
            this.rightButton.setVisibility(4);
        }
        this.rightButton.setOnTouchListener((View.OnTouchListener)new View.OnTouchListener() {
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                NativeKeyboardImpl.this.sendEnteredTextToListener(true, onNativeKeyboardEventListener);
                if (!jsonObject.optBoolean("keepOpenAfterSubmit", false)) {
                    if (NativeKeyboardImpl.this.activity.getCurrentFocus() != null) {
                        ((InputMethodManager)NativeKeyboardImpl.this.activity.getSystemService("input_method")).hideSoftInputFromWindow(NativeKeyboardImpl.this.activity.getCurrentFocus().getWindowToken(), 2);
                    }
                    NativeKeyboardImpl.this.view.requestFocus();
                }
                return true;
            }
        });
    }
    
    public void showMessengerKeyboard(final OnNativeKeyboardEventListener onNativeKeyboardEventListener) {
        if (!this.checkLicense()) {
            onNativeKeyboardEventListener.onError("No valid license found; usage of the native keyboard plugin is restricted to 5 minutes.");
            return;
        }
        if (this._accessorybarContainer == null) {
            onNativeKeyboardEventListener.onError("Call 'showMessenger' first. You can use this method to give focus back to the messenger once its lost.");
            return;
        }
        this.activity.runOnUiThread((Runnable)new Runnable() {
            @Override
            public void run() {
                if (NativeKeyboardImpl.this.editText != null) {
                    NativeKeyboardImpl.this.editText.requestFocus();
                    ((InputMethodManager)NativeKeyboardImpl.this.activity.getSystemService("input_method")).showSoftInput((View)NativeKeyboardImpl.this.editText, 1);
                    NativeKeyboardImpl.this.editText.setSelection(NativeKeyboardImpl.this.editText.getText().length());
                    onNativeKeyboardEventListener.onSuccess(null);
                }
            }
        });
    }
    
    public void hideMessengerKeyboard(final OnNativeKeyboardEventListener onNativeKeyboardEventListener) {
        if (this._accessorybarContainer == null) {
            onNativeKeyboardEventListener.onError("Keyboard wasn't showing.");
            return;
        }
        this.activity.runOnUiThread((Runnable)new Runnable() {
            @Override
            public void run() {
                NativeKeyboardImpl.this.hideKeyboard();
                onNativeKeyboardEventListener.onSuccess(null);
            }
        });
    }
    
    public void showMessenger(final JSONObject jsonObject, final OnNativeKeyboardEventListener onNativeKeyboardEventListener) {
        if (!this.checkLicense()) {
            onNativeKeyboardEventListener.onError("No valid license found; usage of the native keyboard plugin is restricted to 5 minutes.");
            return;
        }
        if (this._accessorybarContainer != null) {
            onNativeKeyboardEventListener.onError("Already showing");
            return;
        }
        this.activity.runOnUiThread((Runnable)new Runnable() {
            @Override
            public void run() {
                try {
                    NativeKeyboardImpl.this.configureLeftButton(jsonObject, onNativeKeyboardEventListener);
                    NativeKeyboardImpl.this.configureRightButton(jsonObject, onNativeKeyboardEventListener);
                    NativeKeyboardImpl.this.configureEditText(jsonObject, onNativeKeyboardEventListener);
                    final LinearLayout linearLayout = new LinearLayout((Context)NativeKeyboardImpl.this.activity);
                    final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(NativeKeyboardImpl.this.view.getLayoutParams());
                    layoutParams.addRule(12);
                    linearLayout.setGravity(80);
                    linearLayout.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
                    linearLayout.setOrientation(0);
                    final LinearLayout linearLayout2 = new LinearLayout((Context)NativeKeyboardImpl.this.activity);
                    linearLayout2.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
                    linearLayout2.setPadding(4, 20, 4, 28);
                    linearLayout2.setGravity(80);
                    this.addBackgroundColorAndBorder(linearLayout2);
                    if (NativeKeyboardImpl.this.leftButton != null) {
                        linearLayout2.addView((View)NativeKeyboardImpl.this.leftButton);
                    }
                    linearLayout2.addView((View)NativeKeyboardImpl.this.editText);
                    if (NativeKeyboardImpl.this.rightButton != null) {
                        linearLayout2.addView((View)NativeKeyboardImpl.this.rightButton);
                    }
                    linearLayout.addView((View)linearLayout2);
                    NativeKeyboardImpl.this._messengerEventListener = onNativeKeyboardEventListener;
                    if (jsonObject.optBoolean("animated", false)) {
                        final TranslateAnimation animation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 0, 160.0f, 0, 0.0f);
                        ((Animation)animation).setDuration(500L);
                        linearLayout.clearAnimation();
                        linearLayout.setAnimation((Animation)animation);
                        linearLayout.startLayoutAnimation();
                    }
                    ViewGroup viewGroup = (ViewGroup)NativeKeyboardImpl.this.view.getParent();
                    if (viewGroup.getChildCount() == 2) {
                        final ViewGroup viewGroup2 = (ViewGroup)viewGroup.getParent();
                        if (viewGroup2.getChildCount() == 1) {
                            viewGroup = viewGroup2;
                        }
                    }
                    viewGroup.addView((View)linearLayout);
                    NativeKeyboardImpl.this._accessorybarContainer = linearLayout;
                    onNativeKeyboardEventListener.onSuccess(new JSONObject().put("ready", true));
                }
                catch (JSONException ex) {
                    onNativeKeyboardEventListener.onError(ex.getMessage());
                }
            }
            
            private void addBackgroundColorAndBorder(final LinearLayout linearLayout) {
                if (Build.VERSION.SDK_INT >= 16) {
                    final GradientDrawable background = new GradientDrawable();
                    background.setShape(0);
                    final int color = Color.parseColor(jsonObject.optString("backgroundColor", "#F6F6F6"));
                    background.setColors(new int[] { color, color });
                    background.setStroke(3, -3355444);
                    linearLayout.setBackground((Drawable)background);
                }
            }
        });
    }
    
    public void updateMessenger(final JSONObject jsonObject, final OnNativeKeyboardEventListener onNativeKeyboardEventListener) {
        if (!this.checkLicense()) {
            onNativeKeyboardEventListener.onError("No valid license found; usage of the native keyboard plugin is restricted to 5 minutes.");
            return;
        }
        if (this._accessorybarContainer == null) {
            onNativeKeyboardEventListener.onError("Call 'showMessenger' first.");
            return;
        }
        this.activity.runOnUiThread((Runnable)new Runnable() {
            @Override
            public void run() {
                if (jsonObject.has("text")) {
                    NativeKeyboardImpl.this.editText.setText((CharSequence)jsonObject.optString("text"));
                }
                if (jsonObject.optBoolean("showKeyboard", false)) {
                    NativeKeyboardImpl.this.editText.requestFocus();
                    ((InputMethodManager)NativeKeyboardImpl.this.activity.getSystemService("input_method")).showSoftInput((View)NativeKeyboardImpl.this.editText, 1);
                }
                final int optInt = jsonObject.optInt("caretIndex", -1);
                if (optInt >= 0 && optInt <= NativeKeyboardImpl.this.editText.getText().length()) {
                    NativeKeyboardImpl.this.editText.setSelection(optInt);
                }
                onNativeKeyboardEventListener.onSuccess(null);
            }
        });
    }
    
    public void hideMessenger(final JSONObject jsonObject, final OnNativeKeyboardEventListener onNativeKeyboardEventListener) {
        if (this._accessorybarContainer == null) {
            onNativeKeyboardEventListener.onError("Already hidden");
            return;
        }
        this.activity.runOnUiThread((Runnable)new Runnable() {
            @Override
            public void run() {
                if (jsonObject.optBoolean("animated", false)) {
                    final TranslateAnimation animation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 0, 0.0f, 0, 160.0f);
                    ((Animation)animation).setDuration(500L);
                    ((Animation)animation).setAnimationListener((Animation.AnimationListener)new Animation.AnimationListener() {
                        public void onAnimationStart(final Animation animation) {
                        }
                        
                        public void onAnimationEnd(final Animation animation) {
                            NativeKeyboardImpl.this.cleanup();
                        }
                        
                        public void onAnimationRepeat(final Animation animation) {
                        }
                    });
                    NativeKeyboardImpl.this._accessorybarContainer.clearAnimation();
                    NativeKeyboardImpl.this._accessorybarContainer.setAnimation((Animation)animation);
                    NativeKeyboardImpl.this._accessorybarContainer.startLayoutAnimation();
                }
                else {
                    NativeKeyboardImpl.this.cleanup();
                }
                onNativeKeyboardEventListener.onSuccess(null);
            }
        });
    }
    
    private void cleanup() {
        ((ViewGroup)this._accessorybarContainer.getParent()).removeView((View)this._accessorybarContainer);
        this._accessorybarContainer = null;
        this.editText = null;
        this.leftButton = null;
        this.rightButton = null;
        this.hideKeyboard();
    }
    
    private void hideKeyboard() {
        if (this.activity.getCurrentFocus() != null) {
            ((InputMethodManager)this.activity.getSystemService("input_method")).hideSoftInputFromWindow(this.activity.getCurrentFocus().getWindowToken(), 2);
        }
    }
    
    private void sendTextChangedEventToListener(final OnNativeKeyboardEventListener onNativeKeyboardEventListener) {
        try {
            onNativeKeyboardEventListener.onSuccess(new JSONObject().put("textChanged", (Object)this.editText.getText().toString()));
        }
        catch (JSONException ex) {
            onNativeKeyboardEventListener.onError(ex.getMessage());
        }
    }
    
    private void sendEnteredTextToListener(final boolean b, final OnNativeKeyboardEventListener onNativeKeyboardEventListener) {
        if (!this.checkLicense()) {
            onNativeKeyboardEventListener.onError("No valid license found; usage of the native keyboard plugin is restricted to 5 minutes.");
            return;
        }
        final String string = this.editText.getText().toString();
        if (!"".equals(string)) {
            try {
                onNativeKeyboardEventListener.onSuccess(new JSONObject().put("messengerRightButtonPressed", b).put("text", (Object)string));
                this.editText.getText().clear();
            }
            catch (JSONException ex) {
                onNativeKeyboardEventListener.onError(ex.getMessage());
            }
        }
    }
    
    private boolean checkLicense() {
        // if (this.licenseIsValid == null) {
        //     final String packageName = this.activity.getPackageName();
        //     final String upperCase = packageName.toUpperCase();
        //     if (upperCase.contains("PROTOTYPE")) {
        //         this.licenseIsValid = true;
        //     }
        //     else if (upperCase.contains("TEST")) {
        //         this.licenseIsValid = true;
        //     }
        //     else if (upperCase.contains("BR.COM.UI2")) {
        //         this.licenseIsValid = true;
        //     }
        //     else if (upperCase.contains("COM.UNTAPPDLLC.APP")) {
        //         this.licenseIsValid = true;
        //     }
        //     else {
        //         final int identifier = this.activity.getResources().getIdentifier("NativeKeyboardPluginLicense", "string", packageName);
        //         final int identifier2 = this.activity.getResources().getIdentifier("NativeKeyboardPluginLicenseAlt", "string", packageName);
        //         try {
        //             final String expectedLicense = this.getExpectedLicense(upperCase);
        //             this.licenseIsValid = (expectedLicense.equals(this.activity.getResources().getString(identifier)) || expectedLicense.equals(this.activity.getResources().getString(identifier2)));
        //         }
        //         catch (Resources.NotFoundException ex) {
        //             this.licenseIsValid = false;
        //         }
        //         catch (NoSuchAlgorithmException ex2) {
        //             this.licenseIsValid = true;
        //         }
        //     }
        // }
        //return this.licenseIsValid || System.currentTimeMillis() - this.appStartTs < 300000L;
        this.licenseIsValid = true;
        return this.licenseIsValid;
    }
    
    private String getExpectedLicense(final String str) throws NoSuchAlgorithmException {
        final MessageDigest instance = MessageDigest.getInstance("SHA-256");
        instance.update(("pack:" + str).getBytes());
        final byte[] digest = instance.digest();
        final StringBuilder sb = new StringBuilder();
        final byte[] array = digest;
        for (int length = array.length, i = 0; i < length; ++i) {
            final String hexString = Integer.toHexString(0xFF & array[i]);
            if (hexString.length() == 1) {
                sb.append('0');
            }
            sb.append(hexString);
        }
        return sb.toString().substring(2, 22);
    }
    
    private void sendMessengerBarHeightChangedEvent(final int previousMessengerBarHeight) {
        if (previousMessengerBarHeight != this.previousMessengerBarHeight) {
            this.previousMessengerBarHeight = previousMessengerBarHeight;
            try {
                this._messengerEventListener.onSuccess(new JSONObject().put("messengerBarHeightChanged", true).put("messengerBarHeight", previousMessengerBarHeight));
            }
            catch (JSONException ex) {}
        }
    }
    
    private class NKOnGlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener
    {
        int previousHeightDiff;
        
        private NKOnGlobalLayoutListener() {
            this.previousHeightDiff = 0;
        }
        
        public void onGlobalLayout() {
            if (NativeKeyboardImpl.this._messengerEventListener == null || NativeKeyboardImpl.this._accessorybarContainer == null) {
                return;
            }
            final Rect rect = new Rect();
            NativeKeyboardImpl.this.rootView.getWindowVisibleDisplayFrame(rect);
            final int height = NativeKeyboardImpl.this.rootView.getRootView().getHeight();
            final int bottom = rect.bottom;
            int y;
            if (Build.VERSION.SDK_INT >= 21) {
                final Display defaultDisplay = NativeKeyboardImpl.this.activity.getWindowManager().getDefaultDisplay();
                final Point point = new Point();
                defaultDisplay.getSize(point);
                y = point.y;
            }
            else {
                y = height;
            }
            final int previousHeightDiff = (int)((y - bottom) / NativeKeyboardImpl.this.density);
            final int n = (int)(NativeKeyboardImpl.this._accessorybarContainer.getChildAt(0).getHeight() / NativeKeyboardImpl.this.density);
            if (previousHeightDiff > 100 && previousHeightDiff != this.previousHeightDiff) {
                try {
                    NativeKeyboardImpl.this._messengerEventListener.onSuccess(new JSONObject().put("keyboardDidShow", true).put("keyboardHeight", previousHeightDiff + n).put("messengerBarHeight", n));
                }
                catch (JSONException ex) {}
            }
            else if (previousHeightDiff != this.previousHeightDiff && this.previousHeightDiff - previousHeightDiff > 100) {
                try {
                    NativeKeyboardImpl.this._messengerEventListener.onSuccess(new JSONObject().put("keyboardDidHide", true));
                }
                catch (JSONException ex2) {}
            }
            NativeKeyboardImpl.this.sendMessengerBarHeightChangedEvent(n);
            this.previousHeightDiff = previousHeightDiff;
        }
    }
}
